import io, hashlib
import pytest
import src.ppvtcrypto.getpass, src.ppvtcrypto.encryptpipe, src.ppvtcrypto
import src.ppvtcrypto.header

PASSWORD = "ÄTçddwr9XáEoðØSSQeóRdòæ5ôWÏÞnÏEµoÇSÿt".encode("UTF-8")
RAW_DATA = b"abc\n"
ENCRYPTED_DATA = \
        b'\x50\x50\x45\x4e\x01\xa1\x9d\xbb\x43\xca\xa9\x11\xf3\x9a\xe6\x6d' \
        + b'\x82\x01\xb3\x6a\xa7\x52\xb2\x92\x2b\x2c\x22\x32\x9e\x1e\x2e\x3f' \
        + b'\x76\xff\x5e\x3e\x75\xfc\x09\x14\xf1\x7c\x6a\x38\x00\x74\x5a\x4f' \
        + b'\xe3\x5f\x17\x89\x2c'
ENCRYPTED_DATA_V2 = \
        b'\x50\x50\x45\x4e\x02\x22\xf1\x97\x9b\xc5\x1a\x65\xe8\x79\x98\xe8' \
        + b'\xba\x79\x2e\x7d\xd6\xf6\x8f\xd0\x96\x82\x70\xda\x41\x61\x2c\x20' \
        + b'\xe6\x5e\x55\x05\xb5\xd1\x47\x3a\x48\x6d\x1e\xf9\x2f\x25\x74\x6d' \
        + b'\xc9\xde\x42\x69\x44\x02\x43\x4a\x94\x07\xa7\x90\x0c\x7b\x87\xab' \
        + b'\xf6\x35\xe4\x40\xd3\x6c\xb5\xd6\xf1\x14\xd4\x31\x5e\xda\x99\x5d' \
        + b'\x94\xa7\x04\x9b\xad\xf3\xa8\xa5\xfe\x23\x89\xc2\x12\xd0\xb7\xd0' \
        + b'\xab\x74\xb1\x52\xd4\x90\x0f\x6b\x56\x36\x67\x6f\x83\x98\x1e\x77' \
        + b'\x07\x2a\x21\x20\x63\x55\x75\xfe\xe7\x4b\x61\xda\xda\x4c\xe4\x74' \
        + b'\x90\x10\xb8\x4d\x97\x1f\xd2\xc0\x82\x3f\xbc\x93\xe8\xfb\x3d\x51' \
        + b'\x1e\xa8\x49\xbf\x92'
ENCRYPTED_DATA_V3 = \
        b'\x50\x50\x45\x4e\x03\x08\x92\x17\x71\x36\xff\x48\x30\xe6\x8e\x10' \
        + b'\x36\xf8\x8c\xc3\x83\xf3\x4d\xe7\xdd\x8d\x10\x36\xa5\x1f\xf0\x89' \
        + b'\x74\x6b\x59\x9b\x7c\xef\x58\x48\xb0\x66\x2c\x0b\x52\xc1\x4e\x3b' \
        + b'\x6d\x90\x71\xa9\xeb\xe7\x22\xfb\x4b\xec\xa4\x6c\x45\xcd\x78\x74' \
        + b'\xd2\xa6\x25\xb4\x8e\xa7\x3c\x64\x0b\x07\xba\xd5\x3b\x7e\x5d\xa0' \
        + b'\xe1\x10\x4a\x2e\x0d\x24\x25\xf8\x1d\x85\x0e\xc4\xce\x45\xbf\x21' \
        + b'\x9c\x0a\x30\x85\xe1\x78\x3b\xe2\x9f\xe8\x9b\xd1\x51\x4f\x1e\xa5' \
        + b'\x19\x7f\x08\x3c\x3c\xee\xe9\x0b\x8b\x19\x5f\x1b\xdc\xee\xe6\xcd' \
        + b'\x79\xda\xa9\x9d\x83\xe4\xf0\xbe\xc1\xa4\x85\xe6\x9f\x44\x08\xb1' \
        + b'\x7f\x67\x0e\x52\x6c'

def create_password_file(tmp_path):
    passwordpath = tmp_path / "password"
    passwordfile = passwordpath.open(mode='wb')
    passwordfile.write(PASSWORD)
    passwordfile.close()
    return str(passwordpath)

def test_decryption_v1(tmp_path):
    passwordfilename = create_password_file(tmp_path)
    ciphertext = io.BytesIO(ENCRYPTED_DATA)
    cleartext = io.BytesIO()

    src.ppvtcrypto.decrypt(passwordfilename, ciphertext, cleartext)

    cleartext.seek(0)
    cleartext_raw = cleartext.read(-1)

    assert cleartext_raw == RAW_DATA

def test_decryption_v2(tmp_path):
    passwordfilename = create_password_file(tmp_path)
    ciphertext = io.BytesIO(ENCRYPTED_DATA_V2)
    cleartext = io.BytesIO()

    src.ppvtcrypto.decrypt(passwordfilename, ciphertext, cleartext)

    cleartext.seek(0)
    cleartext_raw = cleartext.read(-1)

    assert cleartext_raw == RAW_DATA

def test_decryption_v3(tmp_path):
    passwordfilename = create_password_file(tmp_path)
    ciphertext = io.BytesIO(ENCRYPTED_DATA_V3)
    cleartext = io.BytesIO()

    src.ppvtcrypto.decrypt(passwordfilename, ciphertext, cleartext)

    cleartext.seek(0)
    cleartext_raw = cleartext.read(-1)

    assert cleartext_raw == RAW_DATA

def test_symmetry(tmp_path):
    passwordfilename = create_password_file(tmp_path)
    ciphertext = io.BytesIO()
    cleartext = io.BytesIO(RAW_DATA)
    restored_cleartext = io.BytesIO()

    key, passphrase = src.ppvtcrypto.getpass.create_key(ciphertext,
            passwordfilename)
    src.ppvtcrypto.encryptpipe.encrypt(key, cleartext, ciphertext, passphrase)

    ciphertext.seek(0)

    restored_key, version, passphrase = src.ppvtcrypto.getpass.restore_key(
            ciphertext, passwordfilename)
    src.ppvtcrypto.encryptpipe.decrypt(restored_key, version, ciphertext,
            restored_cleartext, passphrase)

    restored_cleartext.seek(0)
    restored_cleartext_raw = restored_cleartext.read(-1)

    assert key == restored_key
    assert restored_cleartext_raw == RAW_DATA


def test_highlevel_symmetry(tmp_path):
    passwordfilename = create_password_file(tmp_path)
    ciphertext = io.BytesIO()
    cleartext = io.BytesIO(RAW_DATA)
    restored_cleartext = io.BytesIO()

    src.ppvtcrypto.encrypt(passwordfilename, False, cleartext, ciphertext)

    ciphertext.seek(0)

    src.ppvtcrypto.decrypt(passwordfilename, ciphertext, restored_cleartext)

    restored_cleartext.seek(0)
    restored_cleartext_raw = restored_cleartext.read(-1)

    assert restored_cleartext_raw == RAW_DATA

def test_highlevel_symmetry_with_buffer_overflow(tmp_path):
    passwordfilename = create_password_file(tmp_path)
    ciphertext = io.BytesIO()
    raw_data = b'deadbeef' * src.ppvtcrypto.encryptpipe.CHUNKSIZE
    cleartext = io.BytesIO(raw_data)
    restored_cleartext = io.BytesIO()

    src.ppvtcrypto.encrypt(passwordfilename, False, cleartext, ciphertext)

    ciphertext.seek(0)

    src.ppvtcrypto.decrypt(passwordfilename, ciphertext, restored_cleartext)

    restored_cleartext.seek(0)
    restored_cleartext_raw = restored_cleartext.read(-1)

    assert restored_cleartext_raw == raw_data

def test_encryption(tmp_path):
    passwordfilename = create_password_file(tmp_path)
    ciphertext = io.BytesIO()
    cleartext = io.BytesIO(RAW_DATA)

    src.ppvtcrypto.encrypt(passwordfilename, False, cleartext, ciphertext)

    ciphertext.seek(0)
    ciphertext_raw = ciphertext.read(-1)

    assert ciphertext_raw[:4] == b'PPEN'
    assert ciphertext_raw[4] == 3
    # magic number, payload version, salt, IV, hash KDF salt, hash salt, one
    # block of data, 512 bits checksum
    assert len(ciphertext_raw) == 4 + 1 + 16 + 16 + 16 + 16 + 16 + 64

def test_verifying_hasher_with_varying_writes():
    salt = b'1' * src.ppvtcrypto.encryptpipe.HASH_KEY_SALT_LEN
    key = b'2' * src.ppvtcrypto.encryptpipe.HASH_KEY_LEN
    hasher = src.ppvtcrypto.encryptpipe.create_cleartext_hasher(salt, key)
    data = (b'a' * 3, b'b' * 64, b'c' * 64, b'd' * 150, b'e' * 2)
    for d in data:
        hasher.update(d)
    expected = hasher.digest()
    verifier = src.ppvtcrypto.encryptpipe.VerifyingHasher(key, salt)
    verified_buffer = []

    for d in data:
        verified_buffer.append(verifier.update(d))
    verified_buffer.append(verifier.update(expected))
    verified_buffer.append(verifier.finalize())

    assert b''.join(data) == b''.join(verified_buffer)

def test_verifying_hasher_with_invalid_hash():
    salt = b'1' * src.ppvtcrypto.encryptpipe.HASH_KEY_SALT_LEN
    key = b'2' * src.ppvtcrypto.encryptpipe.HASH_KEY_LEN
    hasher = src.ppvtcrypto.encryptpipe.create_cleartext_hasher(salt, key)
    data = (b'a' * 3, b'b' * 64, b'c' * 64, b'd' * 150, b'e' * 2)
    for d in data:
        hasher.update(d)
    expected = hasher.digest()
    verifier = src.ppvtcrypto.encryptpipe.VerifyingHasher(key, salt)

    for d in data:
        verifier.update(d)
    verifier.update(expected[:-1])
    verifier.update(b'x')
    with pytest.raises(ValueError):
        verifier.finalize()

def test_verifying_hasher_with_premature_finalize():
    salt = b'1' * src.ppvtcrypto.encryptpipe.HASH_KEY_SALT_LEN
    key = b'2' * src.ppvtcrypto.encryptpipe.HASH_KEY_LEN
    hasher = src.ppvtcrypto.encryptpipe.create_cleartext_hasher(salt, key)
    data = b'a' * 3
    hasher.update(data)
    expected = hasher.digest()
    verifier = src.ppvtcrypto.encryptpipe.VerifyingHasher(key, salt)

    verifier.update(data)
    with pytest.raises(ValueError):
        verifier.finalize()

def test_invalid_header():
    old_header = io.BytesIO(src.ppvtcrypto.header.MAGIC_NUMBER
            + bytes((0,)))
    with pytest.raises(ValueError):
        src.ppvtcrypto.header.check_header(old_header)

def test_too_new_file():
    new_header = io.BytesIO(src.ppvtcrypto.header.MAGIC_NUMBER
            + bytes((src.ppvtcrypto.header.MAX_SUPPORTED_VERSION + 1,)))
    with pytest.raises(ValueError):
        src.ppvtcrypto.header.check_header(new_header)
