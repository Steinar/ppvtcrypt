# Copyright 2019-2022, 2025 Steinar Knutsen
#
# Licensed under the EUPL, Version 1.2 or - as soon they will be approved by the
# European Commission - subsequent versions of the EUPL (the "Licence"); You may
# not use this work except in compliance with the Licence. You may obtain a copy
# of the Licence at:
#
# https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# Licence for the specific language governing permissions and limitations under
# the Licence.

VERSION = 3
MAGIC_NUMBER = b"PPEN"
MIN_SUPPORTED_VERSION = 1
MAX_SUPPORTED_VERSION = 3

def check_header(inputstream):
    header = inputstream.read(5)
    assert len(header) == 5
    if header[:4] != MAGIC_NUMBER:
        raise ValueError("Not a valid ppvtcrypto input file")
    version = header[-1]
    if version == 0:
        raise ValueError("invalid format version number: " + str(version))
    elif version < MIN_SUPPORTED_VERSION:
        raise ValueError("no longer supported format version: " + str(version))
    elif version > MAX_SUPPORTED_VERSION:
        raise ValueError("too new format version: " + str(version))
    return version

def write_header(stream):
    stream.write(MAGIC_NUMBER)
    stream.write(bytes((VERSION,)))
