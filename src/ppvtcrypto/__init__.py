# Copyright 2019-2023, 2025 Steinar Knutsen
#
# Licensed under the EUPL, Version 1.2 or - as soon they will be approved by the
# European Commission - subsequent versions of the EUPL (the "Licence"); You may
# not use this work except in compliance with the Licence. You may obtain a copy
# of the Licence at:
#
# https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# Licence for the specific language governing permissions and limitations under
# the Licence.

__version__ = '8.1.1'

from . import getpass, encryptpipe

def encrypt(passwordfile, doublecheck, cleartext, ciphertext):
    key, passphrase = getpass.create_key(ciphertext, passwordfile, doublecheck)
    encryptpipe.encrypt(key, cleartext, ciphertext, passphrase)

def decrypt(passwordfile, ciphertext, cleartext):
    key, version, passphrase = getpass.restore_key(ciphertext, passwordfile)
    encryptpipe.decrypt(key, version, ciphertext, cleartext, passphrase)
