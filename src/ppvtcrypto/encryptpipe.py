#! /usr/bin/env python3

# Copyright 2019-2022, 2025 Steinar Knutsen
#
# Licensed under the EUPL, Version 1.2 or - as soon they will be approved by the
# European Commission - subsequent versions of the EUPL (the "Licence"); You may
# not use this work except in compliance with the Licence. You may obtain a copy
# of the Licence at:
#
# https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# Licence for the specific language governing permissions and limitations under
# the Licence.

from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.primitives import padding
from cryptography.hazmat.primitives.kdf.scrypt import Scrypt
from cryptography.hazmat.backends import default_backend
import hashlib
from .header import VERSION

CHUNKSIZE = 8 * 1024

HASH_KEY_SALT_LEN = 16
KDF_SALT_LEN = 16
HASH_KEY_LEN = 64
KEYED_HASH_LEN = 64
assert HASH_KEY_LEN <= hashlib.blake2b.MAX_KEY_SIZE
MSG_KEYED_HASH_PERSONALIZATION = \
        b'\xbf\xc0\xc6\xa1\x15\x96\x9b\xd0\xf3\x7f\x15\x06\x9aT\xd1z'
assert len(MSG_KEYED_HASH_PERSONALIZATION) == hashlib.blake2b.PERSON_SIZE

class NopHasher:
    def update(self, data):
        return data
    def finalize(self):
        return b''

class VerifyingHasher:
    def __init__(self, hash_key, keyed_hashing_salt):
        self.hasher = create_cleartext_hasher(keyed_hashing_salt, hash_key)
        self.buffer = b''
    def update_header(self, data):
        self.hasher.update(data)
    def update(self, data):
        if len(data) >= KEYED_HASH_LEN:
            hashed = self.buffer
            self.hasher.update(hashed)
            self.buffer = data
            return hashed
        elif len(self.buffer) >= KEYED_HASH_LEN:
            current = self.buffer + data
            hashed = current[:-KEYED_HASH_LEN]
            self.hasher.update(hashed)
            self.buffer = current[-KEYED_HASH_LEN:]
            return hashed
        else:
            self.buffer += data
            return b''
    def finalize(self):
        if len(self.buffer) < KEYED_HASH_LEN:
            raise ValueError("Insufficient input data")
        last_data = self.buffer[:-KEYED_HASH_LEN]
        self.hasher.update(last_data)
        digest = self.hasher.digest()
        if digest != self.buffer[-KEYED_HASH_LEN:]:
            raise ValueError("Calculated message digest did not match stored")
        return last_data

def create_cleartext_hasher(salt, key):
    return hashlib.blake2b(digest_size=KEYED_HASH_LEN,
            key=key,
            salt=salt,
            person=MSG_KEYED_HASH_PERSONALIZATION)

def create_keyed_hashing_salt():
    f = open("/dev/urandom", "rb")
    salt = f.read(HASH_KEY_SALT_LEN)
    f.close()
    assert len(salt) == HASH_KEY_SALT_LEN
    return salt

def read_keyed_hashing_salt(stream):
    salt = stream.read(HASH_KEY_SALT_LEN)
    assert len(salt) == HASH_KEY_SALT_LEN
    return salt

def create_kdf_salt():
    f = open("/dev/urandom", "rb")
    salt = f.read(KDF_SALT_LEN)
    f.close()
    assert len(salt) == KDF_SALT_LEN
    return salt

def read_kdf_salt(stream):
    salt = stream.read(KDF_SALT_LEN)
    assert len(salt) == KDF_SALT_LEN
    return salt

def derive_hash_key(password, version, salt):
    backend = default_backend()
    kdf = Scrypt(salt=salt, length=HASH_KEY_LEN, n=2**16, r=16, p=1,
            backend=backend)
    key = kdf.derive(password)
    return key

def new_iv():
    rnd = open("/dev/urandom", "rb")
    iv = rnd.read(16)
    rnd.close()
    assert len(iv) == 16
    return iv

def get_cipher(key, iv):
    backend = default_backend()
    cipher = Cipher(algorithms.AES(key), modes.CBC(iv), backend=backend)
    return cipher

def create_hasher(version, passphrase, inputstream):
    if version == 1:
        return NopHasher()
    kdf_salt = read_kdf_salt(inputstream)
    hash_key = derive_hash_key(passphrase, version, kdf_salt)
    keyed_hashing_salt = read_keyed_hashing_salt(inputstream)
    return VerifyingHasher(hash_key, keyed_hashing_salt)

def fresh_hasher(version, passphrase):
    kdf_salt = create_kdf_salt()
    hash_key = derive_hash_key(passphrase, version, kdf_salt)
    keyed_hashing_salt = create_keyed_hashing_salt()
    hasher = create_cleartext_hasher(keyed_hashing_salt, hash_key)
    return kdf_salt, keyed_hashing_salt, hasher

def encrypt(key, inputstream, outputstream, passphrase):
    assert len(key) == 32
    iv = new_iv()
    cipher = get_cipher(key, iv)
    encryptor = cipher.encryptor()
    padder = padding.PKCS7(128).padder()
    outputstream.write(iv)
    kdf_salt, keyed_hashing_salt, hasher = fresh_hasher(VERSION, passphrase)
    hasher.update(bytes((VERSION,)))
    hasher.update(iv)
    outputstream.write(kdf_salt)
    outputstream.write(keyed_hashing_salt)
    clear_text = inputstream.read(CHUNKSIZE)
    while len(clear_text) > 0:
        hasher.update(clear_text)
        outputstream.write(encryptor.update(padder.update(clear_text)))
        clear_text = inputstream.read(CHUNKSIZE)
    outputstream.write(encryptor.update(padder.update(hasher.digest())))
    outputstream.write(encryptor.update(padder.finalize()))
    outputstream.write(encryptor.finalize())
    outputstream.flush()

def end_of_cipher_text(decrypted, unpadder, decryptor, hasher, outputstream):
    remaining = [decrypted]
    remaining.append(hasher.update(unpadder.update(decryptor.finalize())))
    remaining.append(hasher.update(unpadder.finalize()))
    # Verify authentication hash
    remaining.append(hasher.finalize())
    # Write last piece of output here to maximize chance of corrupted
    # output if digest does not match and the above raised an error
    for chunk in remaining:
        outputstream.write(chunk)
    outputstream.flush()

def decrypt(key, version, inputstream, outputstream, passphrase):
    assert len(key) == 32
    iv = inputstream.read(16)
    assert len(iv) == 16
    cipher = get_cipher(key, iv)
    decryptor = cipher.decryptor()
    unpadder = padding.PKCS7(128).unpadder()
    hasher = create_hasher(version, passphrase, inputstream)
    if version > 2:
        hasher.update_header(bytes((version,)))
        hasher.update_header(iv)
    cipher_text = inputstream.read(CHUNKSIZE)
    while len(cipher_text) > 0:
        decrypted = hasher.update(
                unpadder.update(decryptor.update(cipher_text)))
        cipher_text = inputstream.read(CHUNKSIZE)
        if len(cipher_text) == 0:
            end_of_cipher_text(decrypted, unpadder, decryptor, hasher,
                    outputstream)
        else:
            outputstream.write(decrypted)
