#! /usr/bin/env python3

# Copyright 2019-2022 Steinar Knutsen
#
# Licensed under the EUPL, Version 1.2 or - as soon they will be approved by the
# European Commission - subsequent versions of the EUPL (the "Licence"); You may
# not use this work except in compliance with the Licence. You may obtain a copy
# of the Licence at:
#
# https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# Licence for the specific language governing permissions and limitations under
# the Licence.

import getpass
from cryptography.hazmat.primitives.kdf.scrypt import Scrypt
from cryptography.hazmat.backends import default_backend
from .header import check_header, write_header

SALT_LEN = 16

def read_password(passwordfile=None, doublecheck=False):
    password = None
    if passwordfile == None:
        password = getpass.getpass("? ").encode("UTF-8")
        if doublecheck:
            again = getpass.getpass("? ").encode("UTF-8")
            if again != password:
                raise ValueError("passwords did not match")
    else:
        f = open(passwordfile, "rb")
        password = f.readline().rstrip()
        f.close()
    return password

def create_salt():
    f = open("/dev/urandom", "rb")
    salt = f.read(SALT_LEN)
    f.close()
    assert len(salt) == SALT_LEN
    return salt

def read_salt(stream):
    salt = stream.read(SALT_LEN)
    assert len(salt) == SALT_LEN
    return salt

def derive_key(password, version, inputstream):
    salt = read_salt(inputstream)
    backend = default_backend()
    kdf = Scrypt(salt=salt, length=32, n=2**16, r=16, p=1, backend=backend)
    key = kdf.derive(password)
    return key

def derive_new_key_and_write_header(password, outputstream):
    salt = create_salt()
    write_header(outputstream)
    outputstream.write(salt)
    backend = default_backend()
    kdf = Scrypt(salt=salt, length=32, n=2**16, r=16, p=1, backend=backend)
    key = kdf.derive(password)
    return key

def restore_key(inputstream, passwordfile=None):
    password = read_password(passwordfile)
    version = check_header(inputstream)
    key = derive_key(password, version, inputstream)
    return key, version, password

def create_key(outputstream, passwordfile=None, doublecheck=True):
    password = read_password(passwordfile, doublecheck)
    key = derive_new_key_and_write_header(password, outputstream)
    return key, password
